%include "lib.inc"
%define OFFSET 8

section .text
global find_word

; accepts a null-terminated string pointer in rdi
; accepts a dictionary start pointer in rsi
; returns the record address in rax if found
; returns 0 in rax if not found
find_word:
    test rsi, rsi ; dictionary may already be empty
    jz .not_found ; if so, we will never find a word there, so quit
    .loop:
        push rsi ; push dictionary pointer because it is modified by string_equals
        add rsi, OFFSET ; key is OFFSET bytes after the pointer to next word 
        push rdi ; push string pointer because it is modified by string_equals
        call string_equals ; thanks captain obvious
        pop rdi ; pop string pointer
        pop rsi ; pop dictionary pointer

        cmp rax, 1 ; are they equal?
        je .found ; if found, quit and return record address to rax
        mov rsi, [rsi] ; go to next dictionary entry
        test rsi, rsi ; is it null?
    jnz .loop ; if not null, continue
    .not_found:
        xor rax, rax ; nullify the rax
        ret ; urn
    .found:
        mov rax, rsi ; rax equates to pointer to current dictionary entry
        ret ; urn