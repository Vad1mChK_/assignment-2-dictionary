%include "colon.inc"
%include "words.inc"
%include "lib.inc"
%define MAX_KEY_LENGTH 0x100 ; 0xFF symbols and terminator
%define OFFSET 8

extern find_word ; will be loaded during linking
global _start

section .data
    error_not_found_message: db "main: Word not found in dictionary T_T", 10, 0 ; error message
    error_too_long_message: db "main: Key too long (max 255 characters) T_T", 10, 0 ; error message
    key: times MAX_KEY_LENGTH db 0 ; allocate space for supposed key string
section .text
_start:
    mov rdi, key
    mov rsi, MAX_KEY_LENGTH
    call read_word
    test rax, rax
    jz .exit_too_long

    mov rdi, rax
    mov rsi, swimmingly ; in case you wonder, "swimmingly" is the first word in the dictionary
    call find_word
    test rax, rax
    jz .exit_not_found

    add rax, OFFSET
    mov rdi, rax
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi ; because of null terminator
    call print_string
    call exit

    .exit_not_found:
        mov rdi, error_not_found_message
        call print_error
        call exit
    .exit_too_long:
        mov rdi, error_too_long_message
        call print_error
        call exit