%define next_ptr 0

%macro colon 2
%ifnid %2
    %error "Not a correct label."
%else
    %2: dq next_ptr
    %define next_ptr %2
%endif
%ifnstr %1
    %error "Not a correct statement."
%else
    db %1, 0
%endif
%endmacro