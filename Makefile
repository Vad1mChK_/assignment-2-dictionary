ASM=nasm
ASM_FLAGS=-f elf64
LD=ld
RM=rm

%.o: %.asm
	$(ASM) $(ASM_FLAGS) -o $@ $<

main.o: main.asm lib.inc colon.inc words.inc
	$(ASM) $(ASM_FLAGS) -o $@ $<

main: main.o dict.o lib.o
	$(LD) -o $@ $^

.PHONY: clean
clean:
	$(RM) -f *.o