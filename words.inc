; new vocab from "Coda of the Cosmos" from Surreal Memes series https://www.youtube.com/watch?v=6CjtCIRlfJw
; this is quite the fancy vocabulary that could be intimidating to some because these are very archaic words

section .data

colon "abscond", abscond
db 'Abscond -- (verb) to go away suddenly and secretly in order to escape from somewhere.', 10, 0

colon "accursed", accursed
db 'Accursed -- (adjective) very annoying.', 10, 0

colon "drivel", drivel
db 'Drivel -- (noun) nonsense or boring and unnecessary information.', 10, 0

colon "elucidate", elucidate
db 'Elucidate -- (verb) to explain something or make something clear.', 10, 0

colon "enliven", enliven
db 'Enliven -- (verb) to make something more interesting.', 10, 0

colon "extricate", extricate
db 'Extricate -- (verb) to remove something or set something free with difficulty.', 10, 0

colon "malfeasance", malfeasance
db 'Malfeasance -- (noun) an example of dishonest and illegal behaviour, especially by a person in authority.', 10, 0

colon "ramification", ramification
db 'Ramification -- (noun) the possible results of an action.', 10, 0

colon "scoff", scoff
db 'Scoff -- (verb) to laugh and talk about a person or idea in a way that shows that you think they are stupid or silly.', 10, 0

colon "swimmingly", swimmingly
db 'Swimmingly -- (adverb) successfully and without any problems.', 10, 0